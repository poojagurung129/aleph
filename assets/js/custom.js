const currentLocation = location.href;
const footerLi = document.querySelectorAll('.footer_li a');
const menuLendth = footerLi.length;
for (let i = 0; i < menuLendth; i++) {
  // const element = array[i];
  if(footerLi[i].href === currentLocation) {
    footerLi[i].className = "active";
  }
  else {
    footerLi[i].className = " ";
  }
}

$(document).ready(function() {
    $('.banner_slider_image__ul').slick({
        infinite: true,
        dots: false,
        arrows: false,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              // centerPadding: '40px',
              slidesToShow: 2
            }
          },
          {
            breakpoint: 576,
            settings: {
              arrows: false,
              centerMode: true,
              // centerPadding: '40px',
              slidesToShow: 1
            }
          }
        ]
      });

      $('.about_wrapper > ul').slick({
        infinite: true,
        dots: false,
        arrows: false,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 3000,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              // centerPadding: '40px',
              slidesToShow: 2
            }
          },
          {
            breakpoint: 576,
            settings: {
              arrows: false,
              // centerMode: true,
              // centerPadding: '40px',
              slidesToShow: 1
            }
          }
        ]
      });

      $('.banner_slider_image__ul_hp').slick({
        infinite: true,
        dots: false,
        arrows: false,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 2000,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              // centerPadding: '40px',
              slidesToShow: 2
            }
          },
          {
            breakpoint: 576,
            settings: {
              arrows: false,
              centerMode: true,
              // centerPadding: '40px',
              slidesToShow: 1
            }
          }
        ]
      });


      $('.b_details_text h4').matchHeight();

});


const getintouchForm = document.querySelector(".getintouch--form form");
const getintouchInput = document.querySelector(".getintouch--form input[type='email'");
const getintouchBtn = document.querySelector(".getintouch--form button[type='submit'");
// console.log(getintouchForm);
const g = document.createElement("div");
g.classList.add("suscribe_div")
console.log(g);

function suscribe(e) {
  e.preventDefault();
    g.classList.add("done");
    let timer = setTimeout(() =>{
    g.innerHTML = ` Thankyou for Subscribing`
    clearInterval(timer);
  },800)
}

getintouchBtn.addEventListener('click', suscribe);
getintouchForm.after(g)


// const video = document.querySelector(".video-wrapper");
// const bgImageVideo = document.querySelector('.bg-image_video');
// const iframe = document.querySelector("#iframe");
// video.onclick = function() {
//   bgImageVideo.style.display = "none";
//   bgImageVideo.style.transition = "all ease-in-out 0.3s";
// }


let vid =" ";
let vimage=" ";
function playYT(id, image) {
  if(vid!=" " && vimage!=" ") {
    let imageHTML = '<a href="javascript:void(0)" onclick="playYT("'+ vid +'","'+ vimage +' ")><img src="assets/images/'+vimage+'.png" </a>';
    document.getElementById('video_'+vid).innerHTML = imageHTML;
  }
  vid = id;
  vimage=image;

  let html = '<iframe width="100%" height="700" src="https://www.youtube.com/embed/'+id+'?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&mute=1"></iframe>';
  document.getElementById('video_'+id).innerHTML = html;
}